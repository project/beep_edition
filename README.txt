
ABOUT BEEP EDITION
-----------

The Beep Edition theme was developed as a mobile-first responsive theme for Drupal 8.

REQUIRED MODULES
- No required modules for the D8 Release

REQUIRED LIBRARIES
- No required libraries at this time for the D8 Release

REQUIRED Grunt/Sass BITS
- The Grunt build file uses 'grunt-contrib-sass' and 'grunt-contrib-watch', and requires node-bourbon (which should bring 'Neat' with it for grid layouts)

TO USE THE NAVIGATION
---------------------

This is a first release, so I'm still working out the documentation. But I've kept things as simple as possible and will answer questions as quickly as I can.

FOR BEST RESULTS WITH SYSTEM STATUS MESSAGES
--------------------------------------------

I move the 'Status Messages' block to right under the 'Tabs' block in the Content region.